//app is for general control over the application
//and connections between the other components
const APP = {
  keyword: null,
  init: () => {
    //this function runs when the page loads
      APP.setPage();
      document.querySelector('#btnSearch').addEventListener('click', SEARCH.getData);
      document.querySelector('#actors .content').addEventListener('click', ACTORS.showActor);
      STORAGE.loadStorage();
      NAV.handelUrl();
      window.addEventListener('hashchange', NAV.handelUrl);
  },
  setPage: () => {
    //hide other page
    let actors = document.querySelector('#actors');
    let instructions = document.querySelector('#instructions');
    actors.classList.remove('active');
    instructions.classList.add('active');
    let sort = document.querySelector('#sort');
    sort.classList.remove('active');
  }
};

//search is for anything to do with the fetch api
const SEARCH = {
    APP_KEY: `82e848c80fa263ee956d296d3ad746d3`,
    list: [],
    getData(ev){
        ev.preventDefault();
        const baseUrl = `https://api.themoviedb.org/3/search/person?`;
        const searchValue = document.querySelector('#search');
        let keyword = searchValue.value.trim();
        let url= `${baseUrl}api_key=${SEARCH.APP_KEY}&query=${keyword}&language=en-US&page=1`;
        if(keyword) {
        url += keyword;
        fetch(url)
           .then((response) => {
            if (response.ok) {
            return response.json(); 
            } else {
               //Failure to fetch 
               throw new Error(`ERROR: ${resp.status} ${resp.statusText}`);
            }}  
           )
           .then((data) => {
               SEARCH.list = data.results;
               ACTORS.actorPage(SEARCH.list);
               STORAGE.getStorage();
               APP.keyword = keyword;
               history.pushState(null, null, `./index.html#${keyword}`);
          })
          .catch((err) => {
              console.log(error);
          });  
        }
    }

};

//actors is for changes connected to content in the actors section
const ACTORS =  {
  clickedActor: null,
  actorPage: (results) => {
    //hide other pages
    let actors = document.querySelector('#actors');
    actors.classList.add('active');
    let instructions = document.querySelector('#instructions');
    instructions.classList.remove('active');
    let media = document.querySelector('#media');
    media.classList.remove('active');
    let div = document.querySelector('#actors .content');
    div.innerHTML = '';
    let df = document.createDocumentFragment();
    let sort = document.querySelector('#sort');
    sort.classList.add('active');

      
    results.forEach((actor) => {
        if(actor.known_for_department === 'Acting') {
         //actor Card
        let p = document.createElement('p');
        p.classList.add("actorCard");
        p.setAttribute('data-id', `${actor.id}`);

        //actor Image
        let img = document.createElement('img');
        let imgUrl = 'https://image.tmdb.org/t/p/h632';
        img.classList.add("actorImg");
        if(actor.profile_path === null) {
         img.src = "https://farmersmarketcoalition.org/wp-content/uploads/2017/03/anon_person.png";
         img.alt = `${actor.name}'s profile`;
        } else {
        img.src = `${imgUrl + actor.profile_path}`;
        img.alt = `${actor.name}'s profile`;
        };

        //actor Info
        let ul = document.createElement('ul');
        ul.classList.add("actorInfo");
        let li1 = document.createElement('li');
        li1.classList.add("actorName");
        li1.innerText = `${actor.name}`; 
    
        let li2 = document.createElement('li');
        li2.classList.add("actorPopularity");
        let popularity = actor.popularity.toFixed(0);
        if(popularity > 10) {
            li2.innerText = '⭐⭐⭐⭐⭐⭐'; }
        else if(popularity >= 8) {
            li2.innerText = '⭐⭐⭐⭐⭐'; }
        else if (popularity >= 6) {
            li2.innerText = '⭐⭐⭐⭐';}
        else if (popularity >= 4) {
        li2.innerText = '⭐⭐⭐';}
        else if (popularity >= 2) {
            li2.innerText = '⭐⭐'; }
        else { li2.innerText = '⭐'; }
        
         ul.append(li1, li2);
         p.append(img, ul);
         df.append(p);
         div.append(df);
     }  
    }) 
    document.querySelector('.name').addEventListener('click', ACTORS.switchPage);
}, 
switchPage(){
    history.pushState(null, null, `./index.html#${APP.keyword}`);

},
  showActor: (ev) => {
    //find Actor's data-id
    let dataId = ev.target.closest('[data-id]'); 
    let id = JSON.parse(dataId.getAttribute('data-id'));
    if (dataId) {
      ACTORS.clickedActor = SEARCH.list.find((actor) => {
        if (actor.id === id) return true;
      });
    } 
    if (ACTORS.clickedActor) {
    //save movie list
    MEDIA.movieList = ACTORS.clickedActor.known_for; 
    MEDIA.moviePage();
    //change URL
    history.pushState(null, null, `./index.html#${APP.keyword}/${id}`);
    }
  }
 } 

//media is for changes connected to content in the media section
const MEDIA = {
    movieList: [],
    hidePage: () => {
        //hide media page
         history.pushState(null, null, `./index.html#${APP.keyword}`);
         media.classList.remove('active');
         actors.classList.add('active');
         sort.classList.add('active');
    },
    moviePage: () => {
    document.querySelector('.back').addEventListener('click', MEDIA.hidePage);
    
    //hide other pages
    let instructions = document.querySelector('#instructions');
    instructions.classList.remove('active');
    let actors = document.querySelector('#actors');
    actors.classList.remove('active');
    let media = document.querySelector('#media');
    media.classList.add('active');
    let sort = document.querySelector('#sort');
    sort.classList.remove('active');

    let div = document.querySelector('#media .content');
    div.innerHTML ='';
    let df = document.createDocumentFragment();
    let h4 = document.createElement('h4');
    h4.innerHTML = `${ACTORS.clickedActor.name} is best known for`
    let div1 = document.createElement('div');
    div1.setAttribute('class', 'container');
    MEDIA.movieList.forEach((movie) => {

        //movie card
        let p = document.createElement('p');
        p.setAttribute('class', "movieCard");

        //movie image
        let img = document.createElement('img');
        let imgUrl = 'https://image.tmdb.org/t/p/w500';
        img.setAttribute('class', "movieImg");
        if(movie.poster_path === null) {
            img.src = "https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png";
            img.alt = `${movie.title}`;
           } else {
            img.src = `${imgUrl + movie.poster_path}`;
            img.alt = `${movie.title}`;
           };

        //movie Info
        let ul = document.createElement('ul')
        ul.setAttribute('class', "movieInfo");
        let li = document.createElement('li');
        li.setAttribute('class', "movieTitle");
        li.innerText = movie.title || movie.name;

        let li1 = document.createElement('li');
        let date = new Date(`${movie.release_date}`);
        let date1 = new Date(`${movie.first_air_date}`);
        let year = date.getFullYear() || date1.getFullYear();
        li1.innerText = `(${year})`;
        li1.setAttribute('class', "movieDate");

        let li2 = document.createElement('li');
        let score = movie.vote_average;
        li2.innerText = `★ ${score}`;
        li2.setAttribute('class', "movieScore");

        ul.append(li,li1,li2);
        p.append(img, ul);
        div1.append(p);
        df.append(h4, div1);
    })
    div.append(df);
 }
};

//storage is for working with localstorage
const STORAGE = {
  //this will be used in Assign 4
  getStorage: () => {
    const searchValue = document.querySelector('#search');
    let keyword = searchValue.value.trim();
    let key = keyword.toLowerCase();
    let storage = localStorage.getItem(key);
    let val = JSON.stringify(SEARCH.list);
      localStorage.setItem(key, val); 

    STORAGE.loadStorage();
  }, 
  loadStorage() {

  }

};

//nav is for anything connected to the history api and location
const NAV = {
  //this will be used in Assign 4
  handelUrl(){
     
    history.replaceState({}, 'Home', './index.html');
  }

};

//Start everything running

APP.init();
